package advprog.Oricon.bot.controller;

public class Bluray {
    private int rank;
    private String title;
    private String artist;
    private String date;

    public Bluray(int rank, String title, String artist, String date) {
        this.rank = rank;
        this.title = title;
        this.artist = artist;
        this.date = date;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String genre) {
        this.date = genre;
    }

    @Override
    public String toString() {
        return "(" + rank + ") " + title + " - " + artist + " - " + date;
    }
}
