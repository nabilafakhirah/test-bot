package advprog.Oricon.bot.controller;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



@LineMessageHandler
public class Top10BluraysController {

    private static final Logger LOGGER = Logger.getLogger(Top10BluraysController.class.getName());

    @EventMapping
    public TextMessage handleTextMessageEvent(MessageEvent<TextMessageContent> event) throws IOException {
        LOGGER.fine(String.format("TextMessageContent(timestamp='%s',content='%s')",
                event.getTimestamp(), event.getMessage()));
        TextMessageContent content = event.getMessage();
        String contentText = content.getText();

        String replyText = takeData(contentText.split(" ")[2], contentText.split(" ")[3]);
        return new TextMessage(replyText);
    }

    @EventMapping
    public void handleDefaultMessage(Event event) {
        LOGGER.fine(String.format("Event(timestamp='%s',source='%s')",
                event.getTimestamp(), event.getSource()));
    }

    public static String takeData(String category, String searchDate) throws IOException {
        String res = "";

        Document doc;
        try {
            if (category.equalsIgnoreCase("weekly")) {
                doc = Jsoup.connect("https://www.oricon.co.jp/rank/bd/w/"
                        + searchDate
                        + "/").get();
            } else {
                doc = Jsoup.connect("https://www.oricon.co.jp/rank/bd/d/"
                        + searchDate + "/").get();
            }


            ArrayList<Bluray> blurayRankings = new ArrayList<>();

            int counter = 0;
            Elements title = doc.select("section.box-rank-entry .inner a .wrap-text h2");
            for (Element elem : title) {
                Bluray bluray = new Bluray(counter + 1, elem.text(), null, null);
                blurayRankings.add(bluray);
                counter++;
            }

            counter = 0;
            Elements artist = doc.select("section.box-rank-entry .inner a .wrap-text p");
            for (Element elem : artist) {
                blurayRankings.get(counter).setArtist(elem.text());
                counter++;
            }

            counter = 0;
            Elements date = doc.select("section.box-rank-entry .inner a .wrap-text ul li");
            for (Element elem : date) {
                if (elem.text().contains("発売日")) {
                    blurayRankings.get(counter).setDate(elem.text().substring(5));
                    res += blurayRankings.get(counter).toString()
                            + "\n";
                    counter++;
                }
            }

            return res;
        } catch (HttpStatusException e) {
            return "ERROR : 申し訳ありませんが、その日付のグラフは現在利用できません";
        }
    }

}
