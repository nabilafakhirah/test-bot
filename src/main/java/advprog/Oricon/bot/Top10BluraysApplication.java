package advprog.Oricon.bot;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Top10BluraysApplication {

    private static final Logger LOGGER = Logger.getLogger(Top10BluraysApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Application starting ...");
        SpringApplication.run(Top10BluraysApplication.class, args);
    }
}
