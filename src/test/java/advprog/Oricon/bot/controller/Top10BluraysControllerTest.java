package advprog.Oricon.bot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import advprog.Oricon.bot.EventTestUtil;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;



@SpringBootTest(properties = "line.bot.handler.enabled=false")
@ExtendWith(SpringExtension.class)
public class Top10BluraysControllerTest {

    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    @Autowired
    private Top10BluraysController top10BluraysController;

    @Test
    void testContextLoads() {
        assertNotNull(top10BluraysController);
    }

    @Test
    void testHandleTextMessageEvent() throws IOException {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/oricon bluray weekly 2018-05-14");

        TextMessage reply = top10BluraysController.handleTextMessageEvent(event);

        assertEquals("(1) スター・ウォーズ/最後のジェダイ MovieNEX(初回版) - マーク・ハミル - 2018年04月25日\n"
                + "(2) ヴァイオレット・エヴァーガーデン�A - アニメーション - 2018年05月02日\n"
                + "(3) オリエント急行殺人事件 2枚組ブルーレイ&DVD - ケネス・ブラナー - 2018年05月02日\n"
                + "(4) 斉木楠雄のΨ難 豪華版ブルーレイ&DVDセット【初回生産限定】 - 山崎賢人 - 2018年05月02日\n"
                + "(5) ラブライブ!サンシャイン!! 2nd Season 5【特装限定版】 - アニメーション - 2018年04月24日\n"
                + "(6) ミックス。 豪華版Blu-ray - 新垣結衣,瑛太 - 2018年05月02日\n"
                + "(7) GREEN MIND AT BUDOKAN - 秦 基博 - 2018年05月02日\n"
                + "(8) THE IDOLM@STER SideM GREETING TOUR 2017 〜BEYOND THE DREAM〜 LIVE Blu-ray -"
                + " アイドルマスターSideM - 2018年04月25日\n"
                + "(9) SHOGO HAMADA ON THE ROAD 2015-2016“Journey of a Songwriter” - 浜田省吾 - 2018年04月25日\n"
                + "(10) ラブライブ!サンシャイン!! Aqours 2nd LoveLive! HAPPY PARTY TRAIN TOUR Blu-ray"
                + " Memorial BOX - Aqours - 2018年04月25日\n", reply.getText());
    }

    @Test
    void testHandleTextMessageEventError() throws IOException {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/oricon bluray weekly 2018-05-13");

        TextMessage reply = top10BluraysController.handleTextMessageEvent(event);

        assertEquals("ERROR : 申し訳ありませんが、その日付のグラフは現在利用できません", reply.getText());
    }

    @Test
    void testHandleDefaultMessage() {
        Event event = mock(Event.class);

        top10BluraysController.handleDefaultMessage(event);

        verify(event, atLeastOnce()).getSource();
        verify(event, atLeastOnce()).getTimestamp();
    }
}