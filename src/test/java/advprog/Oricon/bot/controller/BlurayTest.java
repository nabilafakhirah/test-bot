package advprog.Oricon.bot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BlurayTest {
    Bluray bluray;

    @BeforeEach
    void setUp() {
        bluray = new Bluray(1, "劇場版「Fate/stay night[Heaven’s Feel]I.presage flower」"
                + " (完全生産限定版)",
                "アニプレックス", "2018-05-09");
    }

    @Test
    void getRankTest() {
        assertEquals(1, bluray.getRank());
    }

    @Test
    void setRankTest() {
        bluray.setRank(2);
        assertEquals(2, bluray.getRank());
    }

    @Test
    void getTitleTest() {
        assertEquals("劇場版「Fate/stay night[Heaven’s Feel]I.presage flower」"
                + " (完全生産限定版)", bluray.getTitle());
    }

    @Test
    void setTitleTest() {
        bluray.setTitle("Fate Stay Night");
        assertEquals("Fate Stay Night", bluray.getTitle());
    }

    @Test
    void getArtistTest() {
        assertEquals("アニプレックス", bluray.getArtist());
    }

    @Test
    void setArtistTest() {
        bluray.setArtist("Aniplex");
        assertEquals("Aniplex", bluray.getArtist());
    }

    @Test
    void getDateTest() {
        assertEquals("2018-05-09", bluray.getDate());
    }

    @Test
    void setDateTest() {
        bluray.setDate("2018-05-10");
        assertEquals("2018-05-10", bluray.getDate());
    }

    @Test
    void toStringTest() {
        assertEquals("(1) 劇場版「Fate/stay night[Heaven’s Feel]I.presage flower」"
                + " (完全生産限定版) - アニプレックス - 2018-05-09", bluray.toString());
    }
}
